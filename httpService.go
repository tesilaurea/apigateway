package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

// E' la struttura principale per l'orchestrazione del servizio esposto

type HttpService struct {
	Method     string   `json:"method"`
	Body       string   `json:"body"`
	Url        string   `json:"url"`
	Uid        string   `json:"uid"`
	Level      string   `json:"level"`
	To         []string `json:"to"`
	ToResource []string `json:"toresource"`
}

/**
 * Costruttore
 */

func makeHttpService(method string, body string, url string, uid string, level string, to []string, toresource []string) HttpService {
	return HttpService{
		Method:     method,
		Body:       body,
		Url:        url,
		Uid:        uid,
		Level:      level,
		To:         to,
		ToResource: toresource,
	}
}

/**
 * Metodi get
 */
func (h HttpService) getMethod() string {
	return h.Method
}

func (h HttpService) getBody() string {
	return h.Body
}

func (h HttpService) getUrl() string {
	return h.Url
}
func (h HttpService) getUid() string {
	return h.Uid
}
func (h HttpService) getLevel() string {
	return h.Level
}

func (h HttpService) getTo() []string {
	return h.To
}

/**
 * Metodi set
 */
func (h HttpService) setMethod(method string) {
	h.Method = method
}

func (h HttpService) setBody(body string) {
	h.Body = body
}

func (h HttpService) setUrl(url string) {
	h.Url = url
}
func (h HttpService) setUid(uid string) {
	h.Uid = uid
}
func (h HttpService) setLevel(level string) {
	h.Level = level
}
func (h HttpService) setTo(to []string) {
	h.To = to
}

//Switch che in realtà non serve con gokit! Forse potrebbe essere utile per integrare servizi di terze parti (per i quali
//non si prevede l'uso di gokit) n.b. ancora non prevedo url con parametri
func (h HttpService) serviceRequest() HttpServiceResponse {
	if "GET" == h.getMethod() {
		fmt.Println(h.getMethod())
		fmt.Println(h.getBody())
		fmt.Println(h.getUrl())
		fmt.Println(h.getTo())
		var response HttpServiceResponse
		resp, err := http.Get(h.getUrl())
		if err != nil {
			panic(err)
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		if err := json.Unmarshal(body, &response); err != nil {
			panic(err)
		}
		fmt.Println(response)
		return response
	} else if "POST" == h.getMethod() {
		fmt.Println(h.getMethod())
		fmt.Println(h.getBody())
		fmt.Println(h.getUrl())
		fmt.Println(h.getTo())
		var response HttpServiceResponse
		/*resp, err := http.Post(h.getUrl(), "application/json; charset=UTF-8", nil)
		if err != nil {
			// handle error
			panic(err)
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		if err := json.Unmarshal(body, &response); err != nil {
			panic(err)
		}*/
		response.Response = ""
		fmt.Println(response)
		return response
	} else if "PUT" == h.getMethod() {
		fmt.Println(h.getMethod())
		fmt.Println(h.getBody())
		fmt.Println(h.getUrl())
		fmt.Println(h.getTo())
		var response HttpServiceResponse
		req, err := http.NewRequest("PUT", h.getUrl(), nil)
		if err != nil {
			panic(err)
		}
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		if err := json.Unmarshal(body, &response); err != nil {
			panic(err)
		}
		fmt.Println(response)
		return response
	} else if "DELETE" == h.getMethod() {
		fmt.Println(h.getMethod())
		fmt.Println(h.getBody())
		fmt.Println(h.getUrl())
		fmt.Println(h.getTo())
		var response HttpServiceResponse
		req, err := http.NewRequest("DELETE", h.getUrl(), nil)
		if err != nil {
			panic(err)
		}

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		if err := json.Unmarshal(body, &response); err != nil {
			panic(err)
		}
		fmt.Println(response)
		return response
	} else {
		panic("Unexpected Method")
	}
}

func replaceConcatParams(url string, param []string) string {
	for _, p := range param {
		a := strings.Split(p, ":")
		k := a[0]
		v := a[1]
		url = strings.Replace(url, "{"+k+"}", v, -1)
	}
	return url
}

func addParameters(url string, parameters []string) string {
	url = url + "?"
	for i, p := range parameters {
		url = url + p
		if i < len(parameters)-1 {
			url = url + "&"
		}
	}
	return url
}

func KAndV(record string) (string, string) {

	a := strings.Split(record, ":")
	k := a[0]
	v := a[1]
	return k, v
}
func MapKAndV(toip []string) map[string]string {
	kvs := make(map[string]string)
	for _, ip := range toip {
		key, value := KAndV(ip)
		kvs[key] = value
	}
	return kvs
}
func ReplaceConcatParameters(url string, parameters string) string {

	a := strings.Split(parameters, "&")
	for _, p := range a {
		i := strings.Split(p, "=")
		k := i[0]
		v := i[1]
		url = strings.Replace(url, "{"+k+"}", v, -1)
	}
	return url
}

func ReplaceAllConcatParameters(tomicroservices []string, istourlconcat []string, tourlconcat []string) map[string]string {

	kvm := MapKAndV(tomicroservices)
	kvisuc := MapKAndV(istourlconcat)
	kvuc := MapKAndV(tourlconcat)

	for k, v := range kvisuc {
		if v == "true" {
			kvm[k] = ReplaceConcatParameters(kvm[k], kvuc[k])
		}
	}
	return kvm
}

func AddUrlParameters(url string, parameters string) string {
	a := strings.Split(parameters, "&")
	var param []string
	for _, p := range a {
		param = append(param, p)
	}
	url = addParameters(url, param)
	return url
}
func AddAllParameters(kvm map[string]string, istoparameters []string, toparameters []string) map[string]string {

	kvisp := MapKAndV(istoparameters)
	kvp := MapKAndV(toparameters)

	for k, v := range kvisp {
		if v == "true" {
			kvm[k] = AddUrlParameters(kvm[k], kvp[k])
		}
	}
	return kvm
}
func ComposeUrl(m map[string]string, ipurl []string, h string) []string {
	var to []string
	kvmip := MapKAndV(ipurl)
	for k, _ := range kvmip {
		to = append(to, h+kvmip[k]+"/"+m[k])
	}
	return to
}
func MapToSlice(m map[string]string) []string {
	var to []string
	to = make([]string, len(m))
	i := 0
	for k, v := range m {
		to[i] = k + ":" + v
		i++
	}
	return to
}

func ConstructDestUrls(tomicroservices []string,
	toip []string,
	istourlconcat []string,
	tourlconcat []string,
	istoparameters []string,
	toparameters []string) []string {

	kvm := ReplaceAllConcatParameters(tomicroservices, istourlconcat, tourlconcat)
	kvm = AddAllParameters(kvm, istoparameters, toparameters)
	to := ComposeUrl(kvm, toip, "http://")
	return to
}
