package main

//Posso pensare di aggiungere altri campi utili.. cosa???
//Potrei ad esempio pensare ad esempio al supporto di più formati diversi della request..
type HttpServiceRequest struct {
	Request string `json:"request"`
}

func makeHttpServiceRequest(request string) HttpServiceRequest {
	return HttpServiceRequest{
		Request: request,
	}
}

/**
 * Metodi get
 */
func (h HttpServiceRequest) getRequest() string {
	return h.Request
}

/**
 * Metodi set
 */
func (h HttpServiceRequest) setRequest(request string) {
	h.Request = request
}
