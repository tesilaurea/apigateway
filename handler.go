package main

import (
	"encoding/json"
	_ "fmt"
	"github.com/gorilla/mux"
	_ "io/ioutil"
	_ "log"
	"net/http"
	_ "strconv"
	_ "time"
)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Ricezione abstract workflow
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Posso pensare di restituire			201 se tutto è ok, e in quel caso eseguo direttamente l'intero Service
//Posso pensare di restituire			404 se è indisponibile il createGraphl() dell'assemblator

//Potrebbe avere senso rimandare l'assegnazione di parametri o altro a posteriori???
//Potrei prevedere un Json body per fare una riscrittura dei parametri in gioco!!

func Service(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	uid := vars["uid"]
	list := CreateSlice(uid) //createList(uid) //createGraph1() // <--- questo non va bene, in teoria la lista viene da una chiamata http a assemblator,
	//	perché se lo va a prendere ad esempio in memoria o da qualche altra parte ad assemblator
	resp := Orchestrate(list, "advanced") //"pipeline")
	var response HttpServiceResponse
	response.Response = resp
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(list); err != nil {
		panic(err)
	}
}
