package main

import (
	"container/list"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

// Recupero le chiamate da fare
func CreateSlice(uid string) []HttpService {
	var httpServiceList []HttpService
	resp, err := http.Get(pathToAssemblator + uid)
	if err != nil {
		// handle error
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(body, &httpServiceList); err != nil {
		panic(err)
	}
	return httpServiceList
}

func createList(uid string) *list.List {
	var httpServiceList []HttpService
	l := list.New()
	l.Init()
	resp, err := http.Get(pathToAssemblator + uid)
	if err != nil {
		// handle error
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)

	if err := json.Unmarshal(body, &httpServiceList); err != nil {
		panic(err)
	}

	for _, h := range httpServiceList {
		l.PushBack(h)
	}
	return l

}

func SetBodyGivenUid(httpServiceResponse HttpServiceResponse, uid string,
	httpServiceList []HttpService) {

	for _, h := range httpServiceList {
		if h.getUid() == uid {
			h.setBody(httpServiceResponse.Response)
		}
	}
}

func Orchestrate(httpServiceList []HttpService, policy string) string {
	//i := 1
	//var v []HttpService
	n := len(httpServiceList)
	switch policy {
	case "pipeline":
		fmt.Println("Gestisco solo pipeline!")
		//var hlist []HttpService
		var hs HttpService
		var rs HttpServiceResponse
		var response string
		var index int
		for i := 0; i < n; i++ {

			for j, h := range httpServiceList {
				level, _ := strconv.Atoi(h.getLevel())
				if i == level {
					hs = h
					index = j
					fmt.Println(level)
					fmt.Println(j)
					fmt.Println("________________")
				}
			}
			rs = hs.serviceRequest()
			if index < len(httpServiceList)-1 {
				httpServiceList[index+1].Body = rs.Response
			}
			if index == len(httpServiceList)-1 {
				response = rs.Response
			}
		}
		return response
	case "advanced":
		fmt.Println("Gestisco anche split di percorsi!")
		var hlist []HttpService
		//var hs HttpService
		//var rs HttpServiceResponse
		//var response string
		for i := 0; i < n; i++ {
			for _, h := range httpServiceList {
				level, _ := strconv.Atoi(h.getLevel())
				if i == level {
					hlist = append(hlist, h)

				}
				for _, l := range hlist {
					rs := l.serviceRequest()
					for _, next := range l.getTo() {
						for z, b := range httpServiceList {
							if b.getUrl() == next {

								httpServiceList[z].Body = rs.Response
								break
							}
						} //else { è un indirizzo esterno e va trattato come tale!}
					}
				}
				hlist = hlist[:0]
			}
		}
		return ""
	default:
		fmt.Println("Non gestisco questo caso!")
		return ""
	}
}

/*var r HttpServiceResponse
//for e := list.Front(); e != nil; e = e.Next() {

	for _, h := range httpServiceList {

		if strconv.Itoa(i) == h.getLevel() {
			v = append(v, h)
			fmt.Println(i)
			//continue
		}

	}

	for _, s := range v {
		fmt.Println(s)
		r = s.serviceRequest()



	}
	v = v[:0]
	v = append(v, a)
	i++
}
if v != nil {
	for _, s := range v {
		fmt.Println(s)
		//s.serviceRequest()
	}
}*/

/*func orchestrate(list *list.List) {
	i := 1
	var v []HttpService
	for e := list.Front(); e != nil; e = e.Next() {

		a := e.Value.(HttpService)
		if strconv.Itoa(i) == a.getLevel() {
			v = append(v, a)
			fmt.Println(i)
			continue
		}
		i++
		for _, s := range v {
			//fmt.Println(s)
			s.serviceRequest()
		}
		v = v[:0]
		v = append(v, a)

	}
	if v != nil {
		for _, s := range v {
			//fmt.Println(s)
			s.serviceRequest()
		}
	}
}*/
