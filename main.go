package main

import (
	"fmt"
	"github.com/gorilla/handlers"
	"log"
	"net/http"
)

/**
 * Main principale del client backend scritto in Go
 */

func main() {
	pathToAssemblator = "http://localhost:8082/getAssemblatedWorkflow/"
	fmt.Println("Inizio la mia esecuzione")
	port := ":8083"
	router := NewRouter()
	log.Fatal(http.ListenAndServe(port, handlers.CORS()(router)))

}
