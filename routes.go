package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

/**
 * Endoint dell'applicazione, con i relativi metodi che vengono invocati
 *
 */

var routes = Routes{
	Route{
		"Service",
		"POST",
		"/service/{uid}",
		Service,
	},
}
