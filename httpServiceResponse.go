package main

type HttpServiceResponse struct {
	Response string `json:"response"`
}

func makeHttpServiceResponse(response string) HttpServiceResponse {
	return HttpServiceResponse{
		Response: response,
	}
}

/**
 * Metodi get
 */
func (h HttpServiceResponse) getRequest() string {
	return h.Response
}

/**
 * Metodi set
 */
func (h HttpServiceResponse) setRequest(response string) {
	h.Response = response
}
